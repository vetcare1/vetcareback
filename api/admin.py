from django.contrib import admin

from api.models.pet import Pet
from api.models.vaccine import Vaccine

# Register your models here.
admin.site.register(Pet)
admin.site.register(Vaccine)