""" Contains vaccine serializer definition """

from rest_framework.serializers import ModelSerializer

from api.models.vaccine import Vaccine

class VaccineSerializer(ModelSerializer):
    """ Defines vaccine serializer behaviour. """
    class Meta:
        """ Defines serializer fields that are being used """
        model = Vaccine
        fields = '__all__'