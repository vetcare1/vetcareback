""" Contains pet serializer definition """

from rest_framework.serializers import ModelSerializer

from api.models.pet import Pet

class PetSerializer(ModelSerializer):
    """ Defines pet serializer behaviour. """
    class Meta:
        """ Defines serializer fields that are being used """
        model = Pet
        fields = '__all__'