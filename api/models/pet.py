""" Contains the Pet model """

from django.db import models


class Pet(models.Model):
    """Contains the fields of the pet model."""
    _ESPECIES_CHOICES = [
        ('dog', 'Dog'),
        ('cat', 'Cat'),
    ]
    owner_name = models.CharField(max_length=100, help_text="Owner's name")
    owner_last_name = models.CharField(max_length=100, help_text="Owner's last name")
    pet_name = models.CharField(max_length=100, help_text="The pet's name")
    pet_last_name = models.CharField(max_length=100, help_text="Pet's last name")
    species = models.CharField(max_length=5, choices=_ESPECIES_CHOICES, help_text="Pet species")
    breed = models.CharField(max_length=100, help_text="Pet Breed")
    age = models.IntegerField(help_text="Pet age")

    class Meta:
        """ Sets human readable name """
        verbose_name = "Pet"

    def __str__(self) -> str:
        return f"{self.pk} {self.pet_name} {self.species}"