""" Contains the Vaccine model """
from django.db import models

from api.models.pet import Pet

class Vaccine(models.Model):
    """Contains the fields of the vaccine model."""
    name = models.CharField(max_length=100, help_text="Vaccine name")
    description = models.TextField(blank=True, null=True, help_text="Detailed description of the vaccine")
    application_date = models.DateField(help_text="Date the vaccine was administered")
    next_date = models.DateField(blank=True, null=True, help_text="Expected date for next application")
    vet = models.CharField(max_length=100, blank=True, null=True, help_text="Name of the veterinarian who administered the vaccine")
    notes = models.TextField(blank=True, null=True, help_text="Additional notes")
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.pk} {self.name}"