from django.urls import path

from .views.pet_view import PetApi
from .views.pet_view import SpecificPetApi
from .views.vaccine_view import VaccineApi
from .views.vaccine_view import SpecificVaccineApi

urlpatterns = [
    path('pet', PetApi.as_view()),
    path('pet/<int:pet_pk>', SpecificPetApi.as_view()),
    path('vaccine', VaccineApi.as_view()),
    path('vaccine/<int:vaccine_pk>', SpecificVaccineApi.as_view()),
]