""" Pet endpoints """

from cerberus import Validator
from django.shortcuts import get_object_or_404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from ..models.pet import Pet
from ..serializers.pet_serializer import PetSerializer

class PetApi(APIView):
    """ Defines general endpoints to pet model """
    def get(self, request):
        """ Returns a pet list.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """
        pets = Pet.objects.all()
        pets_count = Pet.objects.all().count()

        return Response({
            'count': pets_count,
            'data': PetSerializer(pets, many=True).data,
        }, status=status.HTTP_200_OK)
    
    def post(self, request):
        """ Creates a new pet.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """
        validator = Validator({
            "owner_name": {"required": True, "type": "string"},
            "owner_last_name": {"required": True, "type": "string"},
            "pet_name": {"required": True, "type": "string"},
            "pet_last_name": {"required": True, "type": "string"},
            "species": {"required": True, "type": "string"},
            "pet_name": {"required": True, "type": "string"},
            "breed": {"required": True, "type": "string"},
            "age": {"required": True, "type": "integer"},
        })

        if not validator.validate(request.data) or not request.data:
            return Response({
                "code": "invalid_body",
                "detailed": "Request body with invalid format",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        pet = Pet.objects.create(**request.data)
        pet.save()

        return Response({
            'id': pet.pk,
        }, status=status.HTTP_201_CREATED)
    
class SpecificPetApi(APIView):
    """ Defines specific endpoints to pet model """
    def get(self, request, pet_pk):
        """ Returns a pet.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """

        return Response(
            PetSerializer(get_object_or_404(Pet, pk=pet_pk)).data,
            status=status.HTTP_200_OK
        )
    
    def put(self, request, pet_pk):
        """ Updates pet information.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """
        validator = Validator({
            "owner_name": {"required": False, "type": "string"},
            "owner_last_name": {"required": False, "type": "string"},
            "pet_name": {"required": False, "type": "string"},
            "pet_last_name": {"required": False, "type": "string"},
            "species": {"required": False, "type": "string"},
            "pet_name": {"required": False, "type": "string"},
            "breed": {"required": False, "type": "string"},
            "age": {"required": False, "type": "integer"},
        })
    
        if not validator.validate(request.data) or not request.data:
            return Response({
                "code": "invalid_body",
                "detailed": "Request body with invalid format",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)
        
        pet = Pet.objects.filter(pk=pet_pk)
        pet.update(**request.data)
        return Response(status=status.HTTP_200_OK)
    
    def delete(self, request, pet_pk):
        """ Deletes a pet.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """

        get_object_or_404(Pet, pk=pet_pk).delete()
        return Response(status=status.HTTP_200_OK)