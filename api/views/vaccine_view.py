""" Vaccine endpoints """

from api.models.pet import Pet
from cerberus import Validator
from django.shortcuts import get_object_or_404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from ..models.vaccine import Vaccine
from ..serializers.vaccine_serializer import VaccineSerializer

class VaccineApi(APIView):
    """ Defines general endpoints to vaccine model """
    def get(self, request):
        """ Returns a vaccine list.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """
        vaccines = Vaccine.objects.all()
        vaccines_count = Vaccine.objects.all().count()

        return Response({
            'count': vaccines_count,
            'data': VaccineSerializer(vaccines, many=True).data,
        }, status=status.HTTP_200_OK)
    
    def post(self, request):
        """ Creates a new vaccine.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """
        
        validator = Validator({
            "name": {"required": True, "type": "string"},
            "description": {"required": True, "type": "string"},
            "application_date": {
                "required": True, "type": "string",
                'regex': '((19[0-9]{2}|20[0-9]{2}|30[0-9]{2})-'
                         '([0][1-9]|[1][0-2])-'
                         '(10|20|[0-2][1-9]|[3][0-1]))'},
            "next_date":{
                "required": False, "type": "string",
                'regex': '((19[0-9]{2}|20[0-9]{2}|30[0-9]{2})-'
                         '([0][1-9]|[1][0-2])-'
                         '(10|20|[0-2][1-9]|[3][0-1]))'},
            "vet": {"required": True, "type": "string"},
            "notes": {"required": True, "type": "string"},
            "pet": {"required": True, "type": "string"},
        })

        if not validator.validate(request.data) or not request.data:
            return Response({
                "code": "invalid_body",
                "detailed": "Request body with invalid format",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        pet = Pet.objects.filter(pk=request.data["pet"]).first()
        if not pet:
            return Response(status=status.HTTP_404_NOT_FOUND)

        request.data["pet"] = pet
        vaccine = Vaccine.objects.create(**request.data)
        vaccine.save()

        return Response({
            'id': vaccine.pk,
        }, status=status.HTTP_201_CREATED)
    
class SpecificVaccineApi(APIView):
    """ Defines specific endpoints to vaccine model """
    def get(self, request, vaccine_pk):
        """ Returns a vaccine.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """

        return Response(
            VaccineSerializer(get_object_or_404(Vaccine, pk=vaccine_pk)).data,
            status=status.HTTP_200_OK
        )
    
    def put(self, request, vaccine_pk):
        """ Updates vaccine information.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """
        validator = Validator({
            "name": {"required": False, "type": "string"},
            "description": {"required": False, "type": "string"},
            "application_date": {
                "required": False, "type": "string",
                'regex': '((19[0-9]{2}|20[0-9]{2}|30[0-9]{2})-'
                         '([0][1-9]|[1][0-2])-'
                         '(10|20|[0-2][1-9]|[3][0-1]))'},
            "next_date":{
                "required": False, "type": "string",
                'regex': '((19[0-9]{2}|20[0-9]{2}|30[0-9]{2})-'
                         '([0][1-9]|[1][0-2])-'
                         '(10|20|[0-2][1-9]|[3][0-1]))'},
            "vet": {"required": False, "type": "string"},
            "notes": {"required": False, "type": "string"},
            "pet": {"required": False, "type": "string"},
        })
    
        if not validator.validate(request.data):
            return Response({
                "code": "invalid_body",
                "detailed": "Request body with invalid format",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)
        
        vaccine = Vaccine.objects.filter(pk=vaccine_pk)
        vaccine.update(**request.data)
        return Response(status=status.HTTP_200_OK)
    
    def delete(self, request, vaccine_pk):
        """ Deletes a vaccine.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (JSON, int)
                Body response and status code.

        """

        get_object_or_404(Vaccine, pk=vaccine_pk).delete()
        return Response(status=status.HTTP_200_OK)